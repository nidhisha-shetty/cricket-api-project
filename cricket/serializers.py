from rest_framework import serializers, fields
from .models import *

class FirstInningsSerializer(serializers.ModelSerializer):
    class Meta:
        model = FirstInnings
        fields = ['team_name','batsmen','bowler']

class CricketSerializer(serializers.ModelSerializer):
    fi = FirstInningsSerializer(read_only=True, many=False)

    class Meta:
        model = Cricket
        fields = ['fi', 'match_name','venue','date', 'time', 'match_result']


