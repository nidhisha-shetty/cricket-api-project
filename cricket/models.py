from django.db import models


# Create your models here.
class Cricket(models.Model):
    match_name=models.CharField(max_length=50)
    venue=models.CharField(max_length=50)
    date=models.CharField(max_length=50)
    time=models.CharField(max_length=50)
    match_result=models.CharField(max_length=50)
	
class FirstInnings(models.Model):
    team_name=models.CharField(max_length=50)
    batsmen=models.CharField(max_length=50)
    bowler=models.CharField(max_length=50)