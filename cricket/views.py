from django.shortcuts import render
from rest_framework import generics, viewsets
from .models import *
from .serializers import *
# Create your views here.

class cricketview(viewsets.ModelViewSet):
    queryset = Cricket.objects.all()
    serializer_class = CricketSerializer

class fi(viewsets.ModelViewSet):
    queryset = FirstInnings.objects.all()
    serializer_class = FirstInningsSerializer   