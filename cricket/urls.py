# api/urls.py
from django.urls import path, include
from rest_framework import routers
from cricket import views

router =routers.DefaultRouter()
router.register('cricket', views.cricketview)
router.register('fi', views.fi)

urlpatterns = [
    path('', include(router.urls))
]