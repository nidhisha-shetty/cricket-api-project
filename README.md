# About the application
This is a DRF based cricket-api app which provides details such as match
-name, match-venue, match-results, innings-details such as innings-result, bowlers, and batsmen.

## Installing and executing the app
```
> git clone https://gitlab.com/nidhisha-shetty/cricket-api-project
> cd cricket-api-project
> python manage.py runserver
```
